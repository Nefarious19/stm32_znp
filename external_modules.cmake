include(ExternalProject)


ExternalProject_Add(
    etl
    SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/external/etl
    GIT_REPOSITORY https://github.com/ETLCPP/etl.git
    TIMEOUT 10
    UPDATE_COMMAND git pull
    CONFIGURE_COMMAND ""
    BUILD_COMMAND ""
    INSTALL_COMMAND ""
    LOG_DOWNLOAD ON
)

set(etl_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/external/etl/include)
include_directories(${etl_INCLUDE_DIR})

