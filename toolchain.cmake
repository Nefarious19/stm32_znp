set(CMAKE_C_STANDARD 11)
set(CMAKE_CXX_STANDARD 14)
find_package(Git REQUIRED)

set(TOOLCHAIN_PREFIX arm-none-eabi-)
set(TOOLCHAIN_EXTENSION )

set(CMAKE_SYSTEM_NAME               Generic)
set(CMAKE_SYSTEM_PROCESSOR          arm)

# Without that flag CMake is not able to pass test compilation check
set(CMAKE_TRY_COMPILE_TARGET_TYPE   STATIC_LIBRARY)

set(CMAKE_AR                        ${TOOLCHAIN_PREFIX}ar${TOOLCHAIN_EXTENSION})
set(CMAKE_ASM_COMPILER              ${TOOLCHAIN_PREFIX}g++${TOOLCHAIN_EXTENSION})
set(CMAKE_C_COMPILER                ${TOOLCHAIN_PREFIX}gcc${TOOLCHAIN_EXTENSION})
set(CMAKE_CXX_COMPILER              ${TOOLCHAIN_PREFIX}g++${TOOLCHAIN_EXTENSION})
set(CMAKE_LINKER                    ${TOOLCHAIN_PREFIX}g++${TOOLCHAIN_EXTENSION})
set(CMAKE_OBJCOPY                   ${TOOLCHAIN_PREFIX}objcopy${TOOLCHAIN_EXTENSION} CACHE INTERNAL "")
set(CMAKE_RANLIB                    ${TOOLCHAIN_PREFIX}ranlib${TOOLCHAIN_EXTENSION} CACHE INTERNAL "")
set(CMAKE_SIZE                      ${TOOLCHAIN_PREFIX}size${TOOLCHAIN_EXTENSION} CACHE INTERNAL "")
set(CMAKE_STRIP                     ${TOOLCHAIN_PREFIX}strip${TOOLCHAIN_EXTENSION} CACHE INTERNAL "")

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

# Don't know if following setting works also for Ninja
set(CMAKE_VERBOSE_MAKEFILE OFF)

# Remove default static libraries for win32
set(CMAKE_C_STANDARD_LIBRARIES "")
# set(CMAKE_BUILD_TYPE Relase)
# Select cpu type.
SET(CPU_TYPE cortex-m3)

SET(CMAKE_C_FLAGS "-mcpu=${CPU_TYPE} -ffunction-sections -fdata-sections -Wall -fstack-usage --specs=nano.specs --specs=nosys.specs -mfloat-abi=soft -static -mthumb" CACHE INTERNAL "c compiler flags")
SET(CMAKE_CXX_FLAGS "-mcpu=${CPU_TYPE} -ffunction-sections -fdata-sections -fno-exceptions -fno-rtti -fno-use-cxa-atexit -Wall -fstack-usage --specs=nano.specs --specs=nosys.specs -mfloat-abi=soft -static -mthumb" CACHE INTERNAL "cxx compiler flags")
SET(CMAKE_ASM_FLAGS "-mcpu=${CPU_TYPE} -c -x assembler-with-cpp --specs=nano.specs --specs=nosys.specs -mfloat-abi=soft -mthumb" CACHE INTERNAL "asm compiler flags")

SET(CMAKE_C_FLAGS_DEBUG "-O0 -g3" CACHE INTERNAL "c debug compiler flags")
SET(CMAKE_CXX_FLAGS_DEBUG "-O0 -g3" CACHE INTERNAL "cxx debug compiler flags")
SET(CMAKE_ASM_FLAGS_DEBUG "-g3" CACHE INTERNAL "asm debug compiler flags")

SET(CMAKE_C_FLAGS_RELEASE "-O3" CACHE INTERNAL "c release compiler flags")
SET(CMAKE_CXX_FLAGS_RELEASE "-O3" CACHE INTERNAL "cxx release compiler flags")
SET(CMAKE_ASM_FLAGS_RELEASE "" CACHE INTERNAL "asm release compiler flags")

set(LINKER_FILE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/src)
set(LINKER_FILE_FULL_PATH ${LINKER_FILE_DIR}/STM32F103XB_FLASH.ld)
SET(CMAKE_EXE_LINKER_FLAGS "-mcpu=${CPU_TYPE} -T\"${LINKER_FILE_FULL_PATH}\" -Wl,-Map=\"${CMAKE_PROJECT_NAME}.map\" --specs=nano.specs --specs=nosys.specs -Wl,--gc-sections -static -mfloat-abi=soft -mthumb -Wl,--start-group -lc -lm -lstdc++ -lsupc++ -Wl,--end-group"
CACHE INTERNAL "exe link flags")

set(CMAKE_CXX_LINK_EXECUTABLE "<CMAKE_CXX_COMPILER> -o <TARGET> <OBJECTS> <LINK_FLAGS> <LINK_LIBRARIES>")

SET(CMAKE_MODULE_LINKER_FLAGS "-L${TOOLCHAIN_LIB_DIR}" CACHE INTERNAL "module link flags")
SET(CMAKE_SHARED_LINKER_FLAGS "-L${TOOLCHAIN_LIB_DIR}" CACHE INTERNAL "shared link flags")

SET(CMAKE_FIND_ROOT_PATH ${TOOLCHAIN_PREFIX}/${TARGET_TRIPLET} CACHE INTERNAL "cross root directory")
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM BOTH CACHE INTERNAL "")
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY CACHE INTERNAL "")
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY CACHE INTERNAL "")

macro(add_arm_executable target_name)

# Output files
set(elf_file ${target_name}.elf)
set(map_file ${target_name}.map)
set(hex_file ${target_name}.hex)
set(bin_file ${target_name}.bin)
set(lss_file ${target_name}.lss)
set(dmp_file ${target_name}.dmp)

add_executable(${elf_file} ${ARGN})

#generate hex file
add_custom_command(
	OUTPUT ${hex_file}

	COMMAND
		${CMAKE_OBJCOPY} -O ihex ${elf_file} ${hex_file}

	DEPENDS ${elf_file}
)

# #generate bin file
add_custom_command(
	OUTPUT ${bin_file}

	COMMAND
		${CMAKE_OBJCOPY} -O binary ${elf_file} ${bin_file}

	DEPENDS ${elf_file}
)

# #generate extended listing
add_custom_command(
	OUTPUT ${lss_file}

	COMMAND
		${CMAKE_OBJDUMP} -h -S ${elf_file} > ${lss_file}

	DEPENDS ${elf_file}
)

# #generate memory dump
add_custom_command(
	OUTPUT ${dmp_file}

	COMMAND
		${CMAKE_OBJDUMP} -x --syms ${elf_file} > ${dmp_file}

	DEPENDS ${elf_file}
)

add_custom_command(
	TARGET ${elf_file}
	
	POST_BUILD
    COMMAND
		echo "Printing data size\n\n"
	COMMAND 
		${CMAKE_SIZE} --format=berkeley ${elf_file}
	COMMAND
		echo "\n\n"
)

#postprocessing from elf file - generate hex bin etc.
add_custom_target(
	${CMAKE_PROJECT_NAME}
	ALL
	DEPENDS ${hex_file} ${bin_file} ${lss_file} ${dmp_file}
)

add_custom_target(
	flash_st
	DEPENDS ${elf_file}
	COMMAND 
		openocd -f interface/stlink.cfg -f target/stm32f1x.cfg -f ${CMAKE_BINARY_DIR}/stlink_flash.cfg
)

add_custom_target(
	flash_j
	DEPENDS ${elf_file}
	COMMAND 
		JLinkExe -CommandFile ../jlink_commander.jlink
)

set_target_properties(
	${CMAKE_PROJECT_NAME}

	PROPERTIES
		OUTPUT_NAME ${elf_file}
)

endmacro(add_arm_executable)

macro(arm_link_libraries target_name)

target_link_libraries(${target_name}.elf ${ARGN})

endmacro(arm_link_libraries)