#ifndef _FIFO_HEADER
#define _FIFO_HEADER

#include <cstdint>
#include <cstring>

template<typename T, const uint32_t _FIFO_SIZE>
class fifo {
public:
    static constexpr uint32_t FIFO_SIZE = _FIFO_SIZE;

    fifo() : m_top_idx{0ul}, m_bot_idx{0ul}, m_fifo_full{false}, m_fifo_empty{true} {}

    fifo(const fifo& other_fifo) = delete;
    const fifo& operator=(const fifo& other_fifo) = delete;

    const T* push(const T* object_ptr) {
        T* ret_value = nullptr;
        auto next_top_idx = (m_top_idx + 1) % FIFO_SIZE;
        if (next_top_idx == m_bot_idx) {
            m_fifo_full = true;
            return ret_value;
        }

        if (m_fifo_empty)
            m_fifo_empty = false;

        m_array[next_top_idx] = *object_ptr;
        ret_value = &m_array[next_top_idx];
        m_top_idx = next_top_idx;
        return ret_value;
    };

    const T* push(const T& object) {
        T* ret_value = nullptr;
        auto next_top_idx = (m_top_idx + 1) % FIFO_SIZE;
        if (next_top_idx == m_bot_idx) {
            m_fifo_full = true;
            return ret_value;
        }

        if (m_fifo_empty)
            m_fifo_empty = false;

        m_array[m_top_idx] = object;
        ret_value = &m_array[next_top_idx];
        m_top_idx = next_top_idx;
        return ret_value;
    }

    const T* get(void) {
        T* ret_value = nullptr;
        auto next_bot_idx = (m_bot_idx + 1) % FIFO_SIZE;
        if (m_bot_idx == m_top_idx) {
            m_fifo_empty = true;
            return ret_value;
        }

        if (m_fifo_full)
            m_fifo_full = false;

        ret_value = &m_array[m_bot_idx];
        m_bot_idx = next_bot_idx;
        return ret_value;
    }

    bool is_empty(void) { return m_fifo_empty; }

    bool is_full(void) { return m_fifo_full; }

private:
    volatile uint32_t m_top_idx;
    volatile uint32_t m_bot_idx;
    volatile bool m_fifo_full;
    volatile bool m_fifo_empty;
    T m_array[FIFO_SIZE];
};

#endif /* _FIFO_HEADER */
