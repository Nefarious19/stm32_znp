#ifndef _UART_DMA_HEADER
#define _UART_DMA_HEADER

#include "utils/fifo/fifo.hpp"
#include <stm32f1xx.h>

class uart_dma;
typedef void (uart_dma::*uart_dma_method)(UART_HandleTypeDef*);

struct callback_adapter_base {
private:
    uart_dma* m_object_ptr;
    uart_dma_method m_method_ptr;
    pUART_CallbackTypeDef m_c_type_callback;

protected:
    static void invoke(uint32_t context, UART_HandleTypeDef* uart_handle);

public:
    callback_adapter_base(pUART_CallbackTypeDef generated_callback);
    pUART_CallbackTypeDef reserve_slot(uart_dma* object_ptr, uart_dma_method method_ptr);
    void free_slot();
};

template<uint32_t unique_id>
struct callback_adapter : public callback_adapter_base {
    callback_adapter() : callback_adapter_base(&callback_adapter<unique_id>::generated_callback){};
    static void generated_callback(UART_HandleTypeDef* uart_handle) { invoke(unique_id, uart_handle); };
};

struct uart_dma_callback {
    uart_dma_callback(uart_dma* obj, uart_dma_method met);
    uart_dma_callback() = default;
    ~uart_dma_callback() = default;

    const uart_dma_callback& operator=(uart_dma_callback&& other) {
        this->m_alloc_idx = other.m_alloc_idx;
        this->m_uart_callback = other.m_uart_callback;
        other.m_alloc_idx = 0;
        other.m_uart_callback = nullptr;
        return *this;
    };

    uart_dma_callback(uart_dma_callback& other) {
        this->m_alloc_idx = other.m_alloc_idx;
        this->m_uart_callback = other.m_uart_callback;
        other.m_alloc_idx = 0;
        other.m_uart_callback = nullptr;
    };

    bool is_valid() const { return (m_uart_callback != nullptr); };

    pUART_CallbackTypeDef operator()(void) const { return m_uart_callback; };

private:
    uint32_t m_alloc_idx{0};
    pUART_CallbackTypeDef m_uart_callback{nullptr};
};

class uart_dma {
public:
    struct uart_dma_job {
        uint8_t* data_begin;
        uint32_t data_len;
    };

private:
    static constexpr uint32_t RX_BUFFER_SIZE = 512u;
    static constexpr uint32_t TX_BUFFER_SIZE = 512u;
    static constexpr uint32_t m_last_posssible_tx_idx = TX_BUFFER_SIZE - 1;
    static constexpr uint32_t m_last_posssible_rx_idx = RX_BUFFER_SIZE - 1;

public:
    uart_dma() = default;

    void begin(UART_HandleTypeDef* uart_handle = nullptr,
        DMA_HandleTypeDef* tx_dma_handle = nullptr,
        DMA_HandleTypeDef* rx_dma_handle = nullptr);

    void write(uint8_t* data, uint32_t size);

private:
    bool tx_fifo_push_wrapper(uint8_t* ptr, uint32_t size);
    void data_sent_callback(UART_HandleTypeDef* uart_handle);
    void data_received_callback(UART_HandleTypeDef* uart_handle);

    uart_dma_callback txp_callback;
    uart_dma_callback rxp_callback;

    UART_HandleTypeDef* m_uart_handle{nullptr};
    DMA_HandleTypeDef* m_dma_tx_handle{nullptr};
    DMA_HandleTypeDef* m_dma_rx_handle{nullptr};

    uint8_t m_rx_buffer[RX_BUFFER_SIZE] = {0};
    uint8_t m_tx_buffer[TX_BUFFER_SIZE] = {0};

    volatile uint32_t m_buf_tx_front_idx{0};
    volatile uint32_t m_buf_tx_back_idx{0};
    volatile uint32_t m_buf_rx_front_idx{0};
    volatile uint32_t m_buf_rx_back_idx{0};
    volatile uint32_t m_no_of_bytes_in_circular_buffer{0};
    volatile bool m_dma_busy_flag{false};
    fifo<uart_dma_job, 32> m_fifo_tx;
    fifo<uart_dma_job, 32> m_fifo_rx;
};

#endif /* _UART_DMA_HEADER */
