#include "uart_dma.hpp"
#include <etl/delegate.h>
#include <functional>

extern "C" void Error_Handler(void);

static callback_adapter<0x00> adapter0;
static callback_adapter<0x01> adapter1;
static callback_adapter<0x02> adapter2;
static callback_adapter<0x03> adapter3;
static callback_adapter<0x04> adapter4;
static callback_adapter<0x05> adapter5;
static callback_adapter<0x06> adapter6;
static callback_adapter<0x07> adapter7;
static callback_adapter<0x08> adapter8;
static callback_adapter_base* avaliable_slots[] =
    {&adapter0, &adapter1, &adapter2, &adapter3, &adapter4, &adapter5, &adapter6, &adapter7, &adapter8};

callback_adapter_base::callback_adapter_base(pUART_CallbackTypeDef generated_callback)
    : m_c_type_callback{generated_callback} {
    m_method_ptr = nullptr;
    m_object_ptr = nullptr;
};

void callback_adapter_base::invoke(uint32_t context, UART_HandleTypeDef* uart_handle) {
    ((avaliable_slots[context]->m_object_ptr)->*(avaliable_slots[context]->m_method_ptr))(uart_handle);
};

pUART_CallbackTypeDef callback_adapter_base::reserve_slot(uart_dma* object_ptr, uart_dma_method method_ptr) {

    if (m_object_ptr)
        return nullptr;
    m_object_ptr = object_ptr;
    m_method_ptr = method_ptr;
    return m_c_type_callback;
};

void callback_adapter_base::free_slot() {
    m_object_ptr = nullptr;
    m_method_ptr = nullptr;
};

uart_dma_callback::uart_dma_callback(uart_dma* obj, uart_dma_method met) {
    m_alloc_idx = 0;
    m_uart_callback = nullptr;
    for (auto adapter : avaliable_slots) {
        m_uart_callback = adapter->reserve_slot(obj, met);
        if (m_uart_callback)
            break;
        m_alloc_idx++;
    }
};


void uart_dma::begin(UART_HandleTypeDef* uart_handle,
    DMA_HandleTypeDef* tx_dma_handle,
    DMA_HandleTypeDef* rx_dma_handle) {

    if (uart_handle && tx_dma_handle && rx_dma_handle) {
        m_uart_handle = uart_handle;
        m_dma_tx_handle = tx_dma_handle;
        m_dma_rx_handle = rx_dma_handle;
    } else {
        Error_Handler();
    }

    txp_callback = uart_dma_callback(this, &uart_dma::data_sent_callback);
    rxp_callback = uart_dma_callback(this, &uart_dma::data_received_callback);

    HAL_UART_RegisterCallback(m_uart_handle, HAL_UART_TX_COMPLETE_CB_ID, txp_callback());
    HAL_UART_RegisterCallback(m_uart_handle, HAL_UART_RX_COMPLETE_CB_ID, rxp_callback());
}

void uart_dma::data_sent_callback(UART_HandleTypeDef* uart_handle) {
    auto temp = m_fifo_tx.get();
    // uart_handle can only be nullptr if we trigger DMA by hand in case of
    // empty fifo
    if (uart_handle == nullptr) {
        HAL_UART_Transmit_DMA(m_uart_handle, temp->data_begin, temp->data_len);
        m_dma_busy_flag = true;
        return;
    }
    // substract size of last transfer from m_no_of_bytes_in_circular_buffer
    m_no_of_bytes_in_circular_buffer -= uart_handle->TxXferSize;
    // if we still have data to send, send them
    if (temp) {
        HAL_UART_Transmit_DMA(m_uart_handle, temp->data_begin, temp->data_len);
        return;
    }
    // if not, do not trigger dma and clear flags
    m_dma_busy_flag = false;
    return;
}

void uart_dma::data_received_callback(UART_HandleTypeDef* uart_handle) {
    asm("NOP");
}

inline bool uart_dma::tx_fifo_push_wrapper(uint8_t* ptr, uint32_t size) {
    uart_dma_job temp{ptr, size};
    if (!m_dma_busy_flag && m_fifo_tx.is_empty()) {
        m_fifo_tx.push(temp);
        data_sent_callback(nullptr);
        return false;
    }
    return (m_fifo_tx.push(temp) == nullptr);
}

void uart_dma::write(uint8_t* data, uint32_t size) {
    uint32_t new_tx_front_idx = m_buf_tx_front_idx + size;
    uint32_t temp_front_idx = m_buf_tx_front_idx;
    uint8_t* temp = &m_tx_buffer[temp_front_idx];

    auto copy = [](uint8_t*& src, uint8_t*& dest, uint32_t len) -> void {
        while (len--) {
            *dest = *src;
            dest++, src++;
        }
    };

    while ((m_no_of_bytes_in_circular_buffer + size) > TX_BUFFER_SIZE)
        ;

    if (new_tx_front_idx > m_last_posssible_tx_idx) {
        uint32_t xfer_size{0};
        xfer_size = static_cast<uint32_t>(TX_BUFFER_SIZE - m_buf_tx_front_idx);
        copy(data, temp, xfer_size);
        m_buf_tx_front_idx = 0;
        m_no_of_bytes_in_circular_buffer += xfer_size;
        while (tx_fifo_push_wrapper(&m_tx_buffer[temp_front_idx], xfer_size))
            ;
        temp_front_idx = 0;
        size -= xfer_size;
        new_tx_front_idx %= TX_BUFFER_SIZE;
        temp = &m_tx_buffer[temp_front_idx];
    }

    if (size) {
        copy(data, temp, size);
        m_buf_tx_front_idx = new_tx_front_idx;
        m_no_of_bytes_in_circular_buffer += size;
        while (tx_fifo_push_wrapper(&m_tx_buffer[temp_front_idx], size))
            ;
    }
}
