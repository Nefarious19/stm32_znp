#ifndef __PERIPHERALS_INIT_H
#define __PERIPHERALS_INIT_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stm32f1xx.h>
#include "main.h"

void SystemClock_Config(void);
void MX_GPIO_Init(void);
void MX_DMA_Init(void);
void MX_USART1_UART_Init(void);
void MX_I2C1_Init(void);
void MX_USART2_UART_Init(void);

#ifdef __cplusplus
}
#endif

#endif /* __PERIPHERALS_INIT_H */