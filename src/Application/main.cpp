#include "main.h"
#include "peripherals_init.h"
#include <etl/vector.h>

#include "user_drivers/uart_dma/uart_dma.hpp"
#include "utils/fifo/fifo.hpp"

I2C_HandleTypeDef hi2c1;
UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;
DMA_HandleTypeDef hdma_usart1_rx;
DMA_HandleTypeDef hdma_usart1_tx;
DMA_HandleTypeDef hdma_usart2_rx;
DMA_HandleTypeDef hdma_usart2_tx;
uart_dma uart1;

void led_blink(uint32_t state) {
    if (state) {
        HAL_GPIO_DeInit(LED1_GPIO_Port, LED1_Pin);
    } else {
        GPIO_InitTypeDef led = {0};

        led.Mode = GPIO_MODE_OUTPUT_OD;
        led.Pin = LED1_Pin;
        led.Pull = GPIO_NOPULL;
        led.Speed = GPIO_SPEED_HIGH;

        HAL_GPIO_Init(GPIOC, &led);
    }
}

int main(void) {
    HAL_Init();
    SystemClock_Config();
    MX_GPIO_Init();
    MX_DMA_Init();
    MX_USART1_UART_Init();
    MX_I2C1_Init();
    MX_USART2_UART_Init();

    uart1.begin(&huart1, &hdma_usart1_tx, &hdma_usart1_rx);

    char buf[50] = {0};
    while (1) {
        static auto x{0u};
        x++;
        sprintf(buf, "x is equal to %u\r\n", x);
        uart1.write(reinterpret_cast<uint8_t*>(buf), strlen(reinterpret_cast<const char*>(buf)));
        // led_blink(1);
        // HAL_Delay(100);
        // led_blink(0);
        // HAL_Delay(100);
    }
}

void Error_Handler(void) {
    /* USER CODE BEGIN Error_Handler_Debug */
    /* User can add his own implementation to report the HAL error return state */
    __disable_irq();
    while (1) {}
    /* USER CODE END Error_Handler_Debug */
}
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
